
sealed trait LList[+A]{
  def isEmpty:Boolean
  def length:Int
  def ::[B>:A](element:B):LList[B]
  def ++[B>:A](first:LList[B]):LList[B]
  def printElements:String
  override def toString: String = "["+printElements+"]"
  def map[B>:A](f:(B)=>B):LList[B]
  def flatMap[B>:A](f:(B)=>B):LList[B]

}
case class Empty[A]() extends LList[A] {
  override def isEmpty:Boolean = true

  override def length: Int = 0

  override def ::[B>:A](e:B): LList[B] = NonEmpty(e, this)

  override def ++[B>:A](first:LList[B]): LList[B] = first

  override def printElements: String = ""

  override def map[B >: A](f: B => B): LList[B] = this

  override def flatMap[B >: A](f: B => B): LList[B] = this
}

case class NonEmpty[+A](h:A, tl:LList[A]) extends LList[A]{
  def isEmpty: Boolean=false

  def length: Int = 1 + tl.length

  def ::[B>:A](e:B): LList[B] = NonEmpty(e, this)

  def ++[B>:A](first:LList[B]): LList[B] = NonEmpty(h, tl.++(first))

  def printElements: String= if(tl.isEmpty)""+ h else h + " "+ tl.printElements

  override def map[B >: A](f: B => B): LList[B] = NonEmpty(f(h), tl.map(f))

  override def flatMap[B >: A](f: B => B): LList[B] = NonEmpty
}


object LList{
  def apply[A](items:A*): LList[A] =
    if(items.isEmpty) Empty[A]
    else NonEmpty(items.head, apply(items.tail:_*))
}

 object LListApp extends App{

   def stringboy(e:Int):Int={
   e*e
   }
  val list1 = LList(1,2,3,4,5)
  val list2 = LList(6,7,8)
//   println(list1.::(23))
val list3 = LList(LList(1,2), LList(3,4))
   print(list3.flatMap(stringboy))

val b = List
 }